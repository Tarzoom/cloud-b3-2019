# Rendu TP 2 Containerization in-depth

# I. Gestion de conteneurs Docker

* 🌞 Mettre en évidence l'utilisation de chacun des processus liés à Docker
  * `dockerd`, `containerd`, `containerd-shim`
  * analyser qui est le père de qui (en terme de processus, avec leurs PIDs)
  * avec la commande `ps` par exemple
```
[jon@localhost ~]$ ps -ef | grep docker
root      1229     1  0 janv.06 ?      00:00:18 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
root     23861  1227  0 04:21 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/4f4110bb19f10812bfe20b4fa18ec1322d434aed1cf387be81ea2f7ca881091a -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
jon      23935  8473  0 04:22 pts/1    00:00:00 grep --color=auto docker
  ```

  * 🌞 Utiliser l'API HTTP mise à disposition par `dockerd`
    * utiliser un `curl` (ou autre) pour discuter à travers le socker UNIX
    * récupérer la liste des conteneurs
    * récupérer la liste des images disponibles
```
[jon@localhost ~]$ curl -X GET --unix-socket /var/run/docker.sock http:/containers/json
[{"Id":"4f4110bb19f10812bfe20b4fa18ec1322d434aed1cf387be81ea2f7ca881091a","Names":["/charming_wu"],"Image":"alpine","ImageID":"sha256:965ea09ff2ebd2b9eeec88cd822ce156f6674c7e99be082c7efac3c62f3ff652","Command":"sleep 9999","Created":1578367277,"Ports":[],"Labels":{},"State":"running","Status":"Up 14 minutes","HostConfig":{"NetworkMode":"default"},"NetworkSettings":{"Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"47df9b1c6d99be680b264748bab8fa77a1bf943e4830b66b97e5fce0a1c40e6a","EndpointID":"2d30ab18e5116b57ab56d4daf02e6b77cf105571e288d76fb4eb38b71da0658a","Gateway":"172.17.0.1","IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:02","DriverOpts":null}}},"Mounts":[]}]
```

## 1. Namespaces

### A. Exploration manuelle

🌞 Trouver les namespaces utilisés par votre shell.
```
[jon@localhost ~]$ lsns -l
        NS TYPE  NPROCS   PID USER COMMAND
4026531836 pid        3  1879 jon  -bash
4026531837 user       3  1879 jon  -bash
4026531838 uts        3  1879 jon  -bash
4026531839 ipc        3  1879 jon  -bash
4026531840 mnt        3  1879 jon  -bash
4026531956 net        3  1879 jon  -bash
```

### B. `unshare`

🌞 Créer un pseudo-conteneur à la main en utilisant `unshare`
* lancer une commande `unshare`
* `unshare` doit exécuter le processus `bash`
* ce processus doit utiliser des namespaces différents de votre hôte :
  * réseau
  * mount
  * PID
  * user
* prouver depuis votre `bash` isolé que ces namespaces sont bien mis en place
```
[jon@15 ~]$ sudo unshare -n -m -p -u -f
[root@15 jon]# lsns -l
        NS TYPE  NPROCS   PID USER   COMMAND
4026531836 pid       99     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026531837 user     101     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026531838 uts       98     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026531839 ipc      101     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026531840 mnt       93     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026531856 mnt        1    13 root   kdevtmpfs
4026531956 net       98     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026532170 mnt        1   776 chrony /usr/sbin/chronyd
4026532171 mnt        3   802 root   /usr/sbin/NetworkManager --no-daemon
4026532175 mnt        3  1950 root   unshare -n -m -p -u -f
4026532176 uts        3  1950 root   unshare -n -m -p -u -f
4026532177 pid        2  1951 root   -bash
4026532179 net        3  1950 root   unshare -n -m -p -u -f
```
### C. Avec docker

🌞 Trouver dans quels namespaces ce conteneur s'exécute.

```
[jon@15 ~]$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
8603fa36392b        debian              "sleep 99999"       About an hour ago   Up About an hour                        elated_wu
[jon@15 ~]$ PID=$(docker inspect -f {{.State.Pid}} 8603fa36392b)
[jon@15 ~]$ echo $PID
2061
[jon@15 ~]$ sudo ls -al /proc/2061/ns
[sudo] Mot de passe de jon : 
total 0
dr-x--x--x. 2 root root 0 10 janv. 21:45 .
dr-xr-xr-x. 9 root root 0 10 janv. 21:45 ..
lrwxrwxrwx. 1 root root 0 10 janv. 21:46 ipc -> ipc:[4026532179]
lrwxrwxrwx. 1 root root 0 10 janv. 21:46 mnt -> mnt:[4026532177]
lrwxrwxrwx. 1 root root 0 10 janv. 21:45 net -> net:[4026532182]
lrwxrwxrwx. 1 root root 0 10 janv. 21:46 pid -> pid:[4026532180]
lrwxrwxrwx. 1 root root 0 10 janv. 21:46 user -> user:[4026531837]
lrwxrwxrwx. 1 root root 0 10 janv. 21:46 uts -> uts:[4026532178]
```
`Voici les différentes étapes pour trouvé les namespace ou ce conteneur s'éxectute:`
`-Récupérer l'id du conteneur`
`-Récupérer le PID du conteneur`
`-et enfin avec l'id du conteneur lister les namespaces sur lequels il est executer`

### D. `nsenter`

🌞 Utiliser `nsenter` pour rentrer dans les namespaces de votre conteneur en y exécutant un shell
* prouver que vous êtes isolé en terme de réseau, arborescence de processus, points de montage
```
[jon@15 ~]$ sudo nsenter -t 2061 -i -m -n -p -u
mesg: ttyname failed: No such device
root@8603fa36392b:/# 
```
```
root@8603fa36392b:/# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
5: eth0@if6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```
```
root@8603fa36392b:/# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 20:45 ?        00:00:00 sleep 99999
root        30     0  0 22:23 ?        00:00:00 -bash
root       354    30  0 22:27 ?        00:00:00 ps -ef
```
```
root@8603fa36392b:/# df -aTh
Filesystem              Type     Size  Used Avail Use% Mounted on
overlay                 overlay   17G  2.5G   15G  15% /
proc                    proc        0     0     0    - /proc
tmpfs                   tmpfs     64M     0   64M   0% /dev
devpts                  devpts      0     0     0    - /dev/pts
sysfs                   sysfs       0     0     0    - /sys
tmpfs                   tmpfs    1.9G     0  1.9G   0% /sys/fs/cgroup
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/systemd
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/devices
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/cpu,cpuacct
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/freezer
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/cpuset
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/hugetlb
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/net_cls,net_prio
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/perf_event
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/blkio
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/pids
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/memory
mqueue                  mqueue      0     0     0    - /dev/mqueue
shm                     tmpfs     64M     0   64M   0% /dev/shm
/dev/mapper/centos-root xfs       17G  2.5G   15G  15% /etc/resolv.conf
/dev/mapper/centos-root xfs       17G  2.5G   15G  15% /etc/hostname
/dev/mapper/centos-root xfs       17G  2.5G   15G  15% /etc/hosts
proc                    proc        0     0     0    - /proc/bus
proc                    proc        0     0     0    - /proc/fs
proc                    proc        0     0     0    - /proc/irq
proc                    proc        0     0     0    - /proc/sys
proc                    proc        0     0     0    - /proc/sysrq-trigger
tmpfs                   tmpfs    1.9G     0  1.9G   0% /proc/asound
tmpfs                   tmpfs    1.9G     0  1.9G   0% /proc/acpi
tmpfs                   tmpfs     64M     0   64M   0% /proc/kcore
tmpfs                   tmpfs     64M     0   64M   0% /proc/keys
tmpfs                   tmpfs     64M     0   64M   0% /proc/timer_list
tmpfs                   tmpfs     64M     0   64M   0% /proc/timer_stats
tmpfs                   tmpfs     64M     0   64M   0% /proc/sched_debug
tmpfs                   tmpfs    1.9G     0  1.9G   0% /proc/scsi
tmpfs                   tmpfs    1.9G     0  1.9G   0% /sys/firmware
```
### E. Et alors, les namespaces User ?

🌞 Mettez en place la configuration nécessaire pour que Docker utilise les namespaces de type User.
```
[jon@localhost ~]# ps aux | grep sleep
root      8665  0.0  0.0 112732   964 pts/0    R+   14:44   0:00 grep --color=auto sleep
```
``On peut voir que le docker est lancé sous l'utilisateur root pour qu'il utilise un namespace user il faut donc faire un remap j'ai trouvé sur internet un script qui fait ça je vais le détailler si dessous``
```
#!/bin/bash

###############################################################
#  AUTEUR:   Xavier
#
#  DESCRIPTION:  création d'un user spécific pour docker
###############################################################


groupadd -g 500000 dockremap
useradd -u 500000 -g dockremap -s /bin/false dockremap

echo "dockremap:500000:65536" >> /etc/subuid && 
echo "dockremap:500000:65536" >>/etc/subgid

echo "
  {
   \"userns-remap\": \"default\"
  }
" > /etc/docker/daemon.json

systemctl daemon-reload && systemctl restart docker

```
``On créé tout d'abord un groupe puis un utilisateur qui porte le même nom que le groupe ici 'dockremap' on lui donne un shell bin/false de manière a limité c'est capacités, et on edite les fichier subuid et subgid afin d'ajouté l'utilisateur docker. Ensuite on va éditer un fichier .json dans etc/docker (conf docker) pour lui dire d'utilisé le user remap par défaut. Pour finir on recharge et redémarre le deamon docker``


### F. Isolation réseau ? 

* 🌞 lancer un conteneur simple
  * je vous conseille une image `debian` histoire d'avoir des commandes comme `ip a` qui n'existent pas dans une image allégée comme `alpine`)
  * ajouter une option pour partager un port (n'importe lequel), pour voir plus d'informations
    * `docker run -d -p 8888:7777 debian sleep 99999`

```
[jon@localhost ~]$ sudo docker run -d -p 8888:7777 debian sleep 99999
84d4bd5c98684d212d5cb88a0ccd4803fc7ac83fbf4bbe09ad5a2bf5bbb22e35
```
```

[jon@localhost ~]$ sudo docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                    NAMES
84d4bd5c9868        debian              "sleep 99999"       3 seconds ago       Up 3 seconds        0.0.0.0:8888->7777/tcp   adoring_euler
```
* 🌞 vérifier le réseau du conteneur
  * vérifier que le conteneur a bien une carte réseau et repérer son IP
    * c'est une des interfaces de la *veth pair*
  * possible avec un shell dans le conteneur OU avec un `docker inspect` depuis l'hôte
```
[jon@localhost ~]$ sudo docker exec -ti 84d4bd5c9868 /bin/bash
root@84d4bd5c9868:/# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
5: eth0@if6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:2/64 scope link
       valid_lft forever preferred_lft forever
```

* 🌞 vérifier le réseau sur l'hôte
  * vérifier qu'il existe une première carte réseau qui porte une IP dans le même réseau que le conteneur
  * vérifier qu'il existe une deuxième carte réseau, qui est la deuxième interface de la *veth pair*
    * son nom ressemble à *vethXXXXX@ifXX*
 ```
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:0f:e6:1a:11 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:fff:fee6:1a11/64 scope link
       valid_lft forever preferred_lft forever
6: veth07e413f@if5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
    link/ether 9a:4f:5c:c3:6a:d6 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::984f:5cff:fec3:6ad6/64 scope link
       valid_lft forever preferred_lft forever
```
  
* identifier les règles *iptables* liées à la création de votre conteneur  

```
[jon@localhost ~]$ sudo iptables -vnL
[...]
Chain DOCKER (1 references)
 pkts bytes target     prot opt in     out     source               destination
    0     0 ACCEPT     tcp  --  !docker0 docker0  0.0.0.0/0            172.17.0.2           tcp dpt:7777
[...]
```


## 2. Cgroups

### A. Découverte manuelle

🌞 Lancer un conteneur Docker et déduire dans quel cgroup il s'exécute
```
[jon@localhost ~]$ sudo docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS              PORTS               NAMES
07803f385815        debian              "sleep 99999"       About a minute ago   Up About a minute                       vigilant_mclean
```
```
[jon@localhost ~]$ sudo docker top 07803f385815
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                10952               10936               0                   16:06               ?                   00:00:00            sleep 99999
```
```
[jon@localhost ~]$ cat /proc/10952/cgroup
11:devices:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
10:perf_event:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
9:freezer:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
8:cpuacct,cpu:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
7:pids:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
6:net_prio,net_cls:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
5:memory:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
4:blkio:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
3:hugetlb:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
2:cpuset:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
1:name=systemd:/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691
```

### B. Utilisation par Docker

🌞 Lancer un conteneur Docker et trouver 
* la mémoire RAM max qui lui est autorisée
```
[jon@localhost ~]$ cat /sys/fs/cgroup/memory/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691/
memory.kmem.max_usage_in_bytes
8724480
```
* le nombre de processus qu'il peut contenir
```
[jon@localhost ~]$ cat /sys/fs/cgroup/cpu/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691/cpu.shares
10952
```
* explorer un peu de vous-même ce qu'il est possible de faire avec des cgroups  

🌞 Altérer les valeurs cgroups allouées par défaut avec des options de la commandes `docker run` (au moins 3)
```
[jon@localhost ~]$ docker run  --cpu-shares=1024 --memory=0 --cpu-period="100000" -d debian sleep 99999
4a9fc9a9833c78435dce690af725a5280fc833c168689c0fa1f791c72fdcbca2
```
* préciser les options utilisées
```
--cpu-shares=2048 :
Définissez cet indicateur sur une valeur supérieure ou inférieure à la valeur par défaut de 1024 pour augmenter ou réduire le poids du conteneur et lui donner accès à une proportion plus ou moins grande des cycles de processeur de la machine hôte.

--memory=0 :
Quantité maximale de mémoire que le conteneur peut utiliser

--cpu-period="100000" :
Spécifiez la période du planificateur CPU CFS, qui est utilisée avec --cpu-quota. Par défaut, 100 micro-secondes.
```
* prouver en regardant dans `/sys` qu'elles sont utilisées
```
--cpu-shares=2048 :
[jon@localhost ~]$ cat /sys/fs/cgroup/cpu/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691/cpu.shares
2048

--memory=0 :
[jon@localhost ~]$ cat /sys/fs/cgroup/memory/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691/memory.kmem.max_usage_in_bytes
0

--cpu-period="100000" :
[jon@localhost ~]$ cat /sys/fs/cgroup/cpu/docker/948396e12681d314f2cbf5bd244e48b89a4a15dc094cbd289ef50cbc2bc11691/cpu.cfs_period_us
100000

```
## 3. Capabilities

### A. Découverte manuelle

* 🌞 déterminer les capabilities actuellement utilisées par votre shell
```
Current: =
Bounding set =cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,35,36
Securebits: 00/0x0/1'b0
 secure-noroot: no (unlocked)
 secure-no-suid-fixup: no (unlocked)
 secure-keep-caps: no (unlocked)
uid=1000(jon)
gid=1000(jon)
groups=10(wheel),993(docker),1000(jon)
```

🌞 Déterminer les capabilities du processus lancé par un conteneur Docker
* utiliser quelque chose de simple pour le conteneur comme un `docker run -d alpine sleep 99999`
```
[jon@localhost ~]$ cat /proc/18808/status | grep Cap
CapInh: 00000000a80425fb
CapPrm: 00000000a80425fb
CapEff: 00000000a80425fb
CapBnd: 00000000a80425fb
CapAmb: 0000000000000000
```
```
[jon@localhost ~]$ capsh --decode=00000000a80425fb
0x00000000a80425fb=cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap

```
* en utilisant `/proc`
```
[jon@localhost ~]$ cat /proc/18808/status
Name:   sleep
Umask:  0022
State:  S (sleeping)
Tgid:   18808
Ngid:   0
Pid:    18808
PPid:   18793
TracerPid:      0
Uid:    0       0       0       0
Gid:    0       0       0       0
FDSize: 64
Groups: 0 1 2 3 4 6 10 11 20 26 27
VmPeak:     1556 kB
VmSize:     1556 kB
VmLck:         0 kB
VmPin:         0 kB
VmHWM:       248 kB
VmRSS:       248 kB
RssAnon:              44 kB
RssFile:             204 kB
RssShmem:              0 kB
VmData:       20 kB
VmStk:       132 kB
VmExe:       672 kB
VmLib:       232 kB
VmPTE:        12 kB
VmSwap:        0 kB
Threads:        1
SigQ:   0/7261
SigPnd: 0000000000000000
ShdPnd: 0000000000000000
SigBlk: 0000000000000000
SigIgn: 0000000000000000
SigCgt: 0000000000000000
CapInh: 00000000a80425fb
CapPrm: 00000000a80425fb
CapEff: 00000000a80425fb
CapBnd: 00000000a80425fb
CapAmb: 0000000000000000
NoNewPrivs:     0
Seccomp:        2
Speculation_Store_Bypass:       vulnerable
Cpus_allowed:   1
Cpus_allowed_list:      0
Mems_allowed:   00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000001
Mems_allowed_list:      0
voluntary_ctxt_switches:        43
nonvoluntary_ctxt_switches:     7
```


🌞 Jouer avec `ping`
* trouver le chemin absolu de `ping`
```
/bin/ping
```
* récupérer la liste de ses capabilities
```
[jon@localhost ~]$ getcap /bin/ping
/bin/ping = cap_net_admin,cap_net_raw+p
```
* enlever toutes les capabilities
  * en utilisant une liste vide
  * `setcap '' <PATH>`
  ```
  sudo setcap '' /bin/ping
  ``` 
* vérifier que `ping` ne fonctionne plus
```
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 0 received, 100% packet loss, time 1004ms
```
* vérifier avec `strace` que c'est bien l'accès à l'ICMP qui a été enlevé
```
strace ping 8.8.8.8
socket(AF_INET, SOCK_RAW, IPPROTO_ICMP) = -1 EPERM (Operation not permitted)
```

### B. Utilisation par Docker

🌞 lancer un conteneur NGINX qui a le strict nécessaire de capabilities pour fonctionner
`liste des capa necessaire`
```
[jon@localhost ~]$ capsh --decode=00000000a80425fb
0x00000000a80425fb=cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap
```
```
[jon@localhost ~]$ docker run --cap-drop all --cap-add cap_chown --cap-add cap_dac_override --cap-add cap_fowner --cap-add cap_fsetid --cap-add cap_kill --cap-add cap_setgid --cap-add cap_setuid --cap-add cap_setpcap --cap-add cap_net_bind_service --cap-add cap_net_raw --cap-add cap_sys_chroot --cap-add cap_mknod --cap-add cap_audit_write --cap-add cap_setfcap -d -p 80:80 nginx
78d063305de33c03de9e4f2aa342cfa02d22ee42ad8266e3f23f4a8633fb5fa9
```
* prouver qu'il fonctionne
```
[jon@localhost ~]$ curl localhost
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
[jon@localhost ~]$

```

* expliquer toutes les capabilities dont il a besoin
```
cap_chown :
  Apportez des modifications arbitraires aux fichiers UID et GID

cap_dac_override :
  Contournez le fichier pour lire, écrire et exécuter des vérifications d'autorisations.

cap_fowner :
  * Contourner les vérifications d'autorisation sur les opérations qui nécessitent normalement
    l'UID du système de fichiers du processus pour correspondre à l'UID du
    fichier (par exemple, chmod  , utime  ), à l'exclusion de ces opérations
    couvert par CAP_DAC_OVERRIDE et CAP_DAC_READ_SEARCH ;
  * définir des drapeaux d'inode (voir ioctl_iflags  ) sur des fichiers arbitraires;
  * définir des listes de contrôle d'accès (ACL) sur des fichiers arbitraires;
  * ignorer le bit collant de répertoire lors de la suppression de fichiers;
  * modifier les attributs étendus de l' utilisateur sur le répertoire collant appartenant à tout utilisateur;
  * spécifiez O_NOATIME pour les fichiers arbitraires dans open  et fcntl  .

cap_fsetid :
  * Ne pas effacer les bits du mode set-user-ID et set-group-ID lorsqu'un le fichier est modifié;
  * définir le bit set-group-ID pour un fichier dont le GID ne correspond pas
    le système de fichiers ou l'un des GID supplémentaires du processus d'appel.

cap_kill :
  Contourner les contrôles de permission pour envoyer des signaux.
  Cela inclut l'utilisation de l'opération ioctl KDSIGACCEPT 

cap_setgid :
  * Effectuer des manipulations arbitraires des GID de processus et liste GID supplémentaire;
  * forger GID lors du passage des informations d'identification de socket via le domaine UNIX prises;
  * écrire un mappage d'ID de groupe dans un espace de noms d'utilisateur

cap_setuid :
  * Effectuer des manipulations arbitraires des UID de processus;
  * forger l'UID lors du passage des informations d'identification de socket via le domaine UNIX prises;
  * écrire un mappage d'ID utilisateur dans un espace de noms utilisateur

cap_setpcap :
  Si les capacités de fichiers sont prises en charge (c'est-à-dire depuis Linux 2.6.24):
  ajouter toute capacité de l'ensemble de délimitation du thread appelant à
  son ensemble héritable;  supprimer des capacités de l'ensemble de délimitation
  ;  apporter des modifications aux bits sécurisés drapeaux.

  Si les capacités de fichier ne sont pas prises en charge (c'est-à-dire, les noyaux avant
  Linux 2.6.24): accordez ou supprimez toute capacité dans l'appelant
  capacité autorisée définie vers ou à partir de tout autre processus.  (Cette
  La propriété de CAP_SETPCAP n'est pas disponible lorsque le noyau est
  configuré pour prendre en charge les capacités de fichier, car CAP_SETPCAP a
  sémantique entièrement différente pour ces noyaux.)

cap_net_bind_service :
  Lier un socket aux ports privilégiés du domaine Internet (port
  nombres inférieurs à 1024).

cap_net_raw :
  * Utilisez des prises RAW et PACKET;
  * lier à n'importe quelle adresse pour un proxy transparent.

cap_sys_chroot :
  * Utilisez chroot;
  * changer les espaces de noms de montage en utilisant setns

cap_mknod :
  Créez des fichiers spéciaux à l'aide de mknod

cap_audit_write :
  Écrivez les enregistrements dans le journal d'audit du noyau.

cap_setfcap :
  Définissez des capacités arbitraires sur un fichier.


```
